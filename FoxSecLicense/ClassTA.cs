﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FoxSecLicense
{
    public class readlicenseclass
    {
        public static string path = "";
        public string diskName = string.Empty;
        public static string deviceID = string.Empty;
        public static string deviceIDflash = string.Empty;
        public static string CompanyUniqueNr = string.Empty;
        public int Readlicense(string parameter, string path, string ENCRYPTION_KEY)
        {
            Int32 i = default(Int32);
            Int32 intUniqNrIdx = default(Int32);
            Int32 intEncryptLicNrIdx = default(Int32);
            string strUniqNr = string.Empty;
            string strEncryptLicNr = string.Empty;
            string decryptedLicense = string.Empty;
            string line = string.Empty;
            // string sername;
            string[] lines;
            string data = string.Empty;
            string datacomnr = string.Empty;
            string datauser = string.Empty;
            string datadoor = string.Empty;
            string datazones = string.Empty;
            string datacompanies = string.Empty;
            string dataworktime = string.Empty;
            string dataportal = string.Empty;
            string datavideo = string.Empty;
            string datavisitor = string.Empty;
            string datacompaniename = string.Empty;
            string datasmart = string.Empty;

            lines = FileReader.ReadLines(path);
            for (i = 0; i < lines.Length; i++)
            {
                line = lines[i];
                if (line.Contains("[CountRequest]"))
                {
                    break;
                }
                else
                {
                    if (line.Length > 10)
                    {
                        if (line.Contains("CompUniqNr"))
                        {
                            string datacomnr1 = Searcher.TrimEnd(line, "CompUniqNr=");
                            if (deviceIDflash == string.Empty)
                            {
                                datacomnr1 = deviceID;
                                string str = "CompUniqNr=" + datacomnr1;
                                string text = File.ReadAllText(path);
                                text = text.Replace(line, str);
                                //File.WriteAllText(path, text);
                            }
                            else
                            {
                            }
                        }                       
                        else
                        if (line.Contains("Users"))
                        {
                            string linedata = Searcher.TrimEnd(line, "Users=");
                            datauser = Encryption.Decrypt(linedata, true, ENCRYPTION_KEY);
                        }
                        else
                        if (line.Contains("CompanieName"))
                        {
                            string linedata = Searcher.TrimEnd(line, "CompanieName=");
                            datacompaniename = Encryption.Decrypt(linedata, true, ENCRYPTION_KEY);
                        }
                        else
                        if (line.Contains("Doors"))
                        {
                            string linedata = Searcher.TrimEnd(line, "Doors=");
                            datadoor = Encryption.Decrypt(linedata, true, ENCRYPTION_KEY);
                        }
                        else
                        if (line.Contains("Zones"))
                        {
                            string linedata = Searcher.TrimEnd(line, "Zones=");
                            datazones = Encryption.Decrypt(linedata, true, ENCRYPTION_KEY);
                        }
                        else
                        if (line.Contains("Companies"))
                        {
                            string linedata = Searcher.TrimEnd(line, "Companies=");
                            datacompanies = Encryption.Decrypt(linedata, true, ENCRYPTION_KEY);
                        }
                        else
                        if (line.Contains("TimeAndAttendense"))
                        {
                            string linedata = Searcher.TrimEnd(line, "TimeAndAttendense=");
                            dataworktime = Encryption.Decrypt(linedata, true, ENCRYPTION_KEY);
                        }
                        else
                        if (line.Contains("Terminals"))
                        {
                            string linedata = Searcher.TrimEnd(line, "Terminals=");
                            dataportal = Encryption.Decrypt(linedata, true, ENCRYPTION_KEY);
                        }
                        else

                        if (line.Contains("SmartPhoneRegistrators"))
                        {
                            string linedata = Searcher.TrimEnd(line, "SmartPhoneRegistrators=");
                            datasmart = Encryption.Decrypt(linedata, true, ENCRYPTION_KEY);
                        }
                        else
                        if (line.Contains("Video"))
                        {
                            string linedata = Searcher.TrimEnd(line, "Video=");
                            datavideo = Encryption.Decrypt(linedata, true, ENCRYPTION_KEY);
                        }
                        else
                        if (line.Contains("Visitors"))
                        {
                            string linedata = Searcher.TrimEnd(line, "Visitors=");
                            datavisitor = Encryption.Decrypt(linedata, true, ENCRYPTION_KEY);
                        }
                        else
                        if (line.Contains("ValidTo"))
                        {
                            string returndata = Searcher.TrimEnd(line, "ValidTo=");
                        }

                    }
                }
                if (intEncryptLicNrIdx > 0 & intUniqNrIdx > 0)
                    break;
            }
            int Readlicensevale = -1;
            string counter = "0";
            switch (parameter)
            {
                case ("CompUniqNr"):
                    counter = datacomnr;
                    break;

                case "Users":
                    if (string.Compare(datauser.Substring(0, 5), "users", true) == 0)
                    {
                        counter = Searcher.TrimEnd(datauser, "users");
                    }
                    else
                    {
                        counter = datauser;
                    }

                    break;
                case "CompanieName":
                    counter = datacompaniename;
                    break;
                case "Doors":

                    if (string.Compare(datadoor.Substring(0, 5), "doors", true) == 0)
                    {
                        counter = Searcher.TrimEnd(datadoor, "doors");
                    }
                    break;
                case "Zones":
                    if (string.Compare(datazones.Substring(0, 5), "zones", true) == 0)
                    {
                        counter = Searcher.TrimEnd(datazones, "zones");
                    }
                    break;
                case "Companies":
                    counter = datacompanies;
                    break;
                case "TimeAndAttendense":
                    if (string.Compare(dataworktime.Substring(0, 17), "timeandattendense", true) == 0)
                    {
                        counter = Searcher.TrimEnd(dataworktime, "timeandattendense");
                    }
                    break;
                case "Terminals":
                    if (string.Compare(dataportal.Substring(0, 9), "terminals", true) == 0)
                    {
                        counter = Searcher.TrimEnd(dataportal, "terminals");
                    }
                    break;
                case "SmartPhoneRegistrators":
                    if (string.Compare(datasmart.Substring(0, 5), "smart", true) == 0)
                    {
                        counter = Searcher.TrimEnd(datasmart, "smart");
                    }

                    break;
                case "Video":
                    if (string.Compare(datavideo.Substring(0, 5), "video", true) == 0)
                    {
                        counter = Searcher.TrimEnd(datavideo, "video");
                    }
                    break;
                case "Visitors":
                    if (string.Compare(datavisitor.Substring(0, 8), "visitors", true) == 0)
                    {
                        counter = Searcher.TrimEnd(datavisitor, "visitors");
                    }
                    break;
            }

            if (counter != "0")
            {
                if (Regex.IsMatch(counter, @"^\d+$"))
                {
                    int result = Convert.ToInt32(counter);
                    return result;
                }
                else
                    return Readlicensevale;
            }
            else
                return Readlicensevale;
        }

        public int ReadRequest(string parameter, string path)
        {
            Int32 i = default(Int32);
            Int32 intUniqNrIdx = default(Int32);
            Int32 intEncryptLicNrIdx = default(Int32);
            string strUniqNr = string.Empty;
            string strEncryptLicNr = string.Empty;
            string decryptedLicense = string.Empty;
            string line = string.Empty;
            // string sername;
            string[] lines;
          
            string data = string.Empty;
            string datacomnr = string.Empty;
            string datauser = string.Empty;
            string datadoor = string.Empty;
            string datazones = string.Empty;
            string datacompanies = string.Empty;
            string dataworktime = string.Empty;
            string dataportal = string.Empty;
            string datavideo = string.Empty;
            string datavisitor = string.Empty;
            string datacompaniename = string.Empty;
            string datasmart = string.Empty;

            lines = FileReader.ReadLines(path);
            for (i = 0; i < lines.Length; i++)
            {
                line = lines[i];
                if (line.Length > 1)
                {
                    if (line.Contains("CompUniqNr"))
                    {
                        string datacomnr1 = Searcher.TrimEnd(line, "CompUniqNr=");
                        CompanyUniqueNr = datacomnr1;

                        if (deviceIDflash == string.Empty)
                        {
                            datacomnr1 = deviceID;
                            string str = "CompUniqNr=" + datacomnr1;
                            string text = File.ReadAllText(path);
                            text = text.Replace(line, str);
                        }
                        else
                        {
                        }
                    }
                    else
                    if (line.Contains("CompanieName"))
                    {
                        string linedata = Searcher.TrimEnd(line, "CompanieName=");
                        datacompaniename = linedata;
                    }
                    else
                    if (line.Contains("Users"))
                    {
                        string linedata = Searcher.TrimEnd(line, "Users=");
                        datauser = linedata;// Encryption.Decrypt(linedata, true);
                    }
                    else
                    if (line.Contains("CompanieName"))
                    {
                        string linedata = Searcher.TrimEnd(line, "CompanieName=");
                        datacompaniename = linedata;//Encryption.Decrypt(linedata, true);
                    }
                    else
                    if (line.Contains("Doors"))
                    {
                        string linedata = Searcher.TrimEnd(line, "Doors=");
                        datadoor = linedata;//Encryption.Decrypt(linedata, true);
                    }
                    else
                    if (line.Contains("Zones"))
                    {
                        string linedata = Searcher.TrimEnd(line, "Zones=");
                        datazones = linedata;//Encryption.Decrypt(linedata, true);
                    }
                    else
                    if (line.Contains("Companies"))
                    {
                        string linedata = Searcher.TrimEnd(line, "Companies=");
                        datacompanies = linedata;//Encryption.Decrypt(linedata, true);
                    }
                    else
                    if (line.Contains("TimeAndAttendense"))
                    {
                        string linedata = Searcher.TrimEnd(line, "TimeAndAttendense=");
                        dataworktime = linedata;
                    }
                    else
                    if (line.Contains("Terminals"))
                    {
                        string linedata = Searcher.TrimEnd(line, "Terminals=");
                        dataportal = linedata;// Encryption.Decrypt(linedata, true);
                    }
                    else

                    if (line.Contains("SmartPhoneRegistrators"))
                    {
                        string linedata = Searcher.TrimEnd(line, "SmartPhoneRegistrators=");
                        datasmart = linedata;// Encryption.Decrypt(linedata, true);
                    }
                    else
                    if (line.Contains("Video"))
                    {
                        string linedata = Searcher.TrimEnd(line, "Video=");
                        datavideo = linedata;// Encryption.Decrypt(linedata, true);
                    }
                    else
                    if (line.Contains("Visitors"))
                    {
                        string linedata = Searcher.TrimEnd(line, "Visitors=");
                        datavisitor = linedata;// Encryption.Decrypt(linedata, true);
                    }
                    else
                    if (line.Contains("ValidTo"))
                    {
                        string returndata = Searcher.TrimEnd(line, "ValidTo=");
                    }
                }
                if (intEncryptLicNrIdx > 0 & intUniqNrIdx > 0)
                    break;
            }

            int Readlicensevale = -1;
            string counter = "0";
            switch (parameter)
            {
                case ("CompUniqNr"):
                    counter = datacomnr;
                    break;

                case "Users":
                    counter = datauser;
                    break;
                case "CompanieName":
                    counter = datacompaniename;
                    break;
                case "Doors":
                    counter = datadoor;
                    break;
                case "Zones":
                    counter = datazones;
                    break;
                case "Companies":
                    counter = datacompanies;
                    break;
                case "TimeAndAttendense":
                    counter = dataworktime;
                    break;
                case "Terminals":
                    counter = dataportal;
                    break;
                case "SmartPhoneRegistrators":
                    counter = datasmart;
                    break;
                case "Video":
                    counter = datavideo;
                    break;
                case "Visitors":
                    counter = datavisitor;
                    break;
            }

            if (counter != "0")
            {
                if (Regex.IsMatch(counter, @"^\d+$"))
                {
                    int result = Convert.ToInt32(counter);
                    return result;
                }
                else
                    return Readlicensevale;
            }
            else

                return Readlicensevale;
        }

        public int checkrequestexist(string path,string reqtype)
        {
            int tc = 0;
            string[] lines;
            lines = FileReader.ReadLines(path);
            for (int i = 0; i < lines.Length; i++)
            {
                string line = lines[i];
                if (line.Length > 1)
                {
                    if (line.Contains(reqtype))
                    {
                        tc = tc + 1;
                    }
                }
            }
            return tc;
        }

        public string ReadCompNr(string parameter, string path)
        {
            Int32 i = default(Int32);
            Int32 intUniqNrIdx = default(Int32);
            Int32 intEncryptLicNrIdx = default(Int32);
            string strUniqNr = string.Empty;
            string strEncryptLicNr = string.Empty;
            string decryptedLicense = string.Empty;
            string line = string.Empty;
            // string sername;
            string[] lines;         
            string datavalidto = string.Empty;
            string cmpnr = string.Empty;          

            lines = FileReader.ReadLines(path);
            for (i = 0; i < lines.Length; i++)
            {
                line = lines[i];
                if (line.Length > 1)
                {
                    if (line.Contains("CompUniqNr"))
                    {
                        string returndata = Searcher.TrimEnd(line, "CompUniqNr=");
                        cmpnr = returndata;
                    }
                }

                if (intEncryptLicNrIdx > 0 & intUniqNrIdx > 0)
                    break;
            }

            string counter = "0";

            switch (parameter)
            {
                case "ValidTo":
                    counter = datavalidto;
                    break;

                case "CompUniqNr":
                    counter = cmpnr;
                    break;
            }

            if (counter != "0")
            {
                return counter;
            }
            else
            {
                return "-1";
            }
        }

        public string ReadValidTo(string parameter, string path)
        {
            Int32 i = default(Int32);
            Int32 intUniqNrIdx = default(Int32);
            Int32 intEncryptLicNrIdx = default(Int32);
            string strUniqNr = string.Empty;
            string strEncryptLicNr = string.Empty;
            string decryptedLicense = string.Empty;
            string line = string.Empty;
            // string sername;
            string[] lines;
            string datavalidto = string.Empty;
            string cmpnr = string.Empty;

            lines = FileReader.ReadLines(path);
            for (i = 0; i < lines.Length; i++)
            {
                line = lines[i];
                if (line.Length > 1)
                {
                    if (line.Contains("CompUniqNr"))
                    {
                        string returndata = Searcher.TrimEnd(line, "CompUniqNr=");
                        cmpnr = returndata;
                    }
                    if (line.Contains("ValidTo"))
                    {
                        string returndata = Searcher.TrimEnd(line, "ValidTo=");
                        datavalidto = returndata;
                    }
                }

                if (intEncryptLicNrIdx > 0 & intUniqNrIdx > 0)
                    break;
            }

            string counter = "0";

            switch (parameter)
            {
                case "ValidTo":
                    counter = datavalidto;
                    break;

                case "CompUniqNr":
                    counter = cmpnr;
                    break;
            }

            if (counter != "0")
            {
                return counter;
            }
            else
            {
                return "-1";
            }
        }

    }
}
