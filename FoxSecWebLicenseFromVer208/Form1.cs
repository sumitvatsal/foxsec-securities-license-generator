﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using System.Management;
using System.ServiceProcess;
using System.Globalization;
using FoxSecLicense;
using System.Configuration;
using System.Security.Cryptography;

namespace FoxSecWebLicense
{
    public partial class Form1 : Form
    {
        public static string path;
        public static string pathtxt;
        public static int CompanieName;
        public static int Users;
        public static int Companies;
        public static int Doors;
        public static int Zones;
        public static int TimeAndAttendense;
        public static int Visitors;
        public static int Video;
        public static int Terminals;
        public static int Smart;
        public static int Path;
        public static string ValidTo;
        public static string CompUniqNr;
        public static int linreqflag = 0;
        public static int liccountflag = 0;

        public static string datacomnr;
        public static string rTime = string.Empty;
        public readlicenseclass TAtest = new readlicenseclass();
        public static bool appIsLocked = true;
        public static string deviceID = string.Empty;
        public static string deviceIDflash = string.Empty;
        private const RegexOptions regexOptions =
        RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.ExplicitCapture;
        private const string dateRegexp = "(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])";
        private static readonly Regex dateRegex =
        new Regex(dateRegexp, regexOptions);
        private const string digitsRegexPattern = @"^[0-9]+$";
        private static readonly Regex digitsRegex = new Regex(digitsRegexPattern, regexOptions);
        public static string ENCRYPTION_KEY = "A456E4DA104F960563A66DDC";
        private static readonly string CR = Environment.NewLine;
        public static string serialnember = string.Empty;
        public static string licenseFilePath = string.Empty;//"C:\\FoxSec\\Web\\FoxSecLicense.ini";
        public string diskName = string.Empty;
        public string confirmPassword = "hard12";
        private static string appPath = string.Empty;
        private static string error = string.Empty;
        public static string Licensenumber;
        internal const string LICENSE_SERNUM =
       "[LicenseCounts]\r\nCompUniqNr={0}";
        private static object l = new object();
        internal const string COMPANIE_NAME =
           "CompanieName={0}";
        internal const string COUNT_CountUsers =
           "Users={0}";
        internal const string COUNT_CountDoors =
           "Doors={0}";
        internal const string ZONES =
           "Zones={0}";
        internal const string COMPANIES =
           "Companies={0}";
        internal const string WORKTIME =
           "TimeAndAttendense={0}";
        internal const string VALIDTO =
           "ValidTo={0}";
        internal const string PORTAL =
           "Terminals={0}";
        internal const string SMART =
           "SmartPhoneRegistrators={0}";

        internal const string VIDEO =
           "Video={0}";
        internal const string VISITORS =
          "Visitors={0}";

        internal const string LICENSE_SERNUMR =
 "\r\n\r\n[CountRequest]\r\nCompanieName={0}";

        internal const string COUNT_CountUsersR =
           "Users={0}";
        internal const string COUNT_CountDoorsR =
           "Doors={0}";
        internal const string ZONESR =
           "Zones={0}";
        internal const string COMPANIESR =
           "Companies={0}";
        internal const string WORKTIMER =
           "TimeAndAttendense={0}";
        internal const string VALIDTOR =
           "ValidTo={0}";
        internal const string PORTALR =
           "Terminals={0}";
        internal const string SMARTR =
         "SmartPhoneRegistrators={0}";
        internal const string VIDEOR =
           "Video={0}";
        internal const string VISITORSR =
          "Visitors={0}";

        string NEW_LINE = System.Environment.NewLine;
        string companiename = "CompanieName";
        string comnr = "CompUniqNr";
        string user = "Users";
        string door = "Doors";
        string zones = "Zones";
        string companies = "Companies";
        string worktime = "TimeAndAttendense";
        string portal = "Terminals";
        string smart = "SmartPhoneRegistrators";
        string video = "Video";
        string visitor = "Visitors";
        string validto = "ValidTo";
        public static string drivename = string.Empty;
        public static int validateread = 0;
        // string path = pathtxt;
        public Form1()
        {
            InitializeComponent();
            RecognizeAppState();
            companiename = "CompanieName";
            comnr = "CompUniqNr";
            user = "Users";
            door = "Doors";
            zones = "Zones";
            companies = "Companies";
            worktime = "TimeAndAttendense";
            portal = "Terminals";
            smart = "SmartPhoneRegistrators";
            video = "Video";
            visitor = "Visitors";

            GetSerFlashDisk();

            if (String.IsNullOrEmpty(path))
            {
                GetSerHardDisk();
            }
            if (!File.Exists(path))
            {
                FileWriter.CreateFile(path);
                List<string> list = new List<string>();
                string dataCompUniqnr = string.Empty;
                string dataCompUniqnr1 = string.Empty;
                if (deviceIDflash == string.Empty)
                { GetSerHardDisk(); }
                if (deviceID == String.Empty)
                {
                    dataCompUniqnr = String.Format(LICENSE_SERNUM, deviceIDflash, true);
                    dataCompUniqnr1 = Encryption.Encrypt(deviceIDflash, true, ENCRYPTION_KEY);
                    list.Add(dataCompUniqnr1.Substring(0, 2));
                }
                else
                {
                    dataCompUniqnr = String.Format(LICENSE_SERNUM, deviceID, true);
                    dataCompUniqnr1 = Encryption.Encrypt(deviceID, true, ENCRYPTION_KEY);
                    list.Add(dataCompUniqnr1.Substring(0, 2));
                }
                try
                {
                    using (StreamWriter stream = new StreamWriter(path, false))
                    {
                        stream.WriteLine(dataCompUniqnr);
                        stream.WriteLine("CompanieName=");
                        stream.WriteLine("Users=");
                        stream.WriteLine("Doors=");
                        stream.WriteLine("Zones=");
                        stream.WriteLine("Companies=");
                        stream.WriteLine("TimeAndAttendense=");
                        stream.WriteLine("Terminals=");
                        stream.WriteLine("SmartPhoneRegistrators=");
                        stream.WriteLine("Video=");
                        stream.WriteLine("Visitors=");
                        stream.WriteLine("License Number=");
                        stream.WriteLine("ValidTo=");
                        stream.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
            }

            label16.Visible = false;
            FileInfo sFile = new FileInfo(path);
            bool fileExist = sFile.Exists;

            if (fileExist == true)
            {
                label10.Text = "Read from existing Lisence";
                //textBox6.Text = confirmPassword;
                path = (path).ToString();
                textBox11.Text = path;
                //  ReadLic();
            }
            else
            {
                label10.Text = "Lisence is default";
                textBox1.Text = "100";
                textBox2.Text = "10";
                textBox3.Text = "50";
                textBox4.Text = "1";
                textBox5.Text = "0";
                textBox10.Text = "0";
                textBox9.Text = "0";
                textBox8.Text = "0";
                textBox12.Text = "0";
                textBox11.Text = path;
            }
            dateTimePicker1.Value = DateTime.Today.AddYears(1000);
            textBox11.WordWrap = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool chkpath = checkwritefilepath();
            if (chkpath == false)
            {
                MessageBox.Show("Entered license path doesn't exists!!");
                return;
            }
            path = textBox11.Text;
            string CompUniqNr = TAtest.ReadValidTo(comnr, path);
            liccountflag = TAtest.checkrequestexist(path, "[CountRequest]");
            if (CompUniqNr == "")
            {
                DialogResult result = MessageBox.Show("Do you want to generate Unique Id from drive?", "Generate", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                if (result.Equals(DialogResult.OK))
                {
                    string devid = "";
                    GetSerFlashDisk1();
                    if (deviceIDflash == string.Empty)
                    { GetSerHardDisk(); }
                    if (deviceID == String.Empty)
                    {
                        devid = deviceIDflash;
                    }
                    else
                    {
                        devid = deviceID;
                    }
                    label16.Text = devid;
                    string[] lines = FileReader.ReadLines(path);
                    string line = "";
                    try
                    {
                        using (StreamWriter stream = new StreamWriter(textBox11.Text, false))
                        {
                            for (int i = 0; i < lines.Length; i++)
                            {
                                line = lines[i];
                                if (line.Contains("CompUniqNr"))
                                {
                                    line = "CompUniqNr=" + devid;
                                    stream.WriteLine(line);
                                }
                                else
                                {
                                    stream.WriteLine(line);
                                }
                            }
                            stream.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return;
                    }
                    linreqflag = 1;
                }
                else
                {
                    return;
                }
            }
            else
            {
                label16.Text = CompUniqNr;
                linreqflag = 1;
            }

            path = textBox11.Text;
            string newPassword = textBox6.Text;//"hard12"
            string confirmPassword = "hard12";

            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(confirmPassword) && !string.IsNullOrEmpty(confirmPassword) && newPassword.Equals(confirmPassword))
            {
                string dataCompUniqnr = string.Empty;
                string dataCompUniqnr1 = string.Empty;
                string uniqkey = "";

                dataCompUniqnr = String.Format("CompUniqNr=" + label16.Text, true);
                uniqkey = ENCRYPTION_KEY + CompUniqNr;
                dataCompUniqnr1 = Encryption.Encrypt(label16.Text, true, uniqkey);
                list.Add(dataCompUniqnr1.Substring(0, 2));

                string strcompanename = textBox7.Text;
                string dataCompanieName = String.Format(COMPANIE_NAME, Encryption.Encrypt(strcompanename, true, uniqkey));
                string dataCompanieName1 = Encryption.Encrypt(textBox7.Text, true, uniqkey);
                list.Add(dataCompanieName1.Substring(0, 2));

                string struser = "users" + textBox1.Text;
                string dataCountUsers = String.Format(COUNT_CountUsers, Encryption.Encrypt(struser, true, uniqkey));
                string dataCountUsers1 = Encryption.Encrypt(textBox1.Text, true, uniqkey);
                list.Add(dataCountUsers1.Substring(0, 2));

                string strdoor = "doors" + textBox2.Text;
                string dataCountDoors = String.Format(COUNT_CountDoors, Encryption.Encrypt(strdoor, true, uniqkey));
                string dataCountDoors1 = Encryption.Encrypt(textBox2.Text, true, uniqkey);
                list.Add(dataCountDoors1.Substring(1, 3));

                string strzones = "zones" + textBox3.Text;
                string dataZones = String.Format(ZONES, Encryption.Encrypt(strzones, true, uniqkey));
                string dataZones1 = Encryption.Encrypt(textBox3.Text, true, uniqkey);
                list.Add(dataZones1.Substring(2, 4));

                string dataCompanies = String.Format(COMPANIES, Encryption.Encrypt(textBox4.Text, true, uniqkey));
                string dataCompanies1 = Encryption.Encrypt(textBox4.Text, true, uniqkey);
                list.Add(dataCompanies1.Substring(1, 3));

                string strWorktime = "timeandattendense" + textBox5.Text;
                string dataWorktime = String.Format(WORKTIME, Encryption.Encrypt(strWorktime, true, uniqkey));
                string ddataWorktime1 = Encryption.Encrypt(textBox5.Text, true, uniqkey);
                list.Add(ddataWorktime1.Substring(0, 2));

                string strportal = "terminals" + textBox8.Text;
                string dataportal = String.Format(PORTAL, Encryption.Encrypt(strportal, true, uniqkey));
                string ddataportal1 = Encryption.Encrypt(textBox8.Text, true, uniqkey);
                list.Add(ddataportal1.Substring(0, 2));

                string strvideo = "video" + textBox9.Text;
                string datavideo = String.Format(VIDEO, Encryption.Encrypt(strvideo, true, uniqkey));
                string ddatavideo1 = Encryption.Encrypt(textBox9.Text, true, uniqkey);
                list.Add(ddatavideo1.Substring(0, 2));

                string strsmart = "smart" + textBox12.Text;
                string datasmart = String.Format(SMART, Encryption.Encrypt(strsmart, true, uniqkey));
                string ddatasmart1 = Encryption.Encrypt(textBox12.Text, true, uniqkey);
                list.Add(ddatasmart1.Substring(0, 2));

                string strvisitor = "visitors" + textBox10.Text;
                string datavisitor = String.Format(VISITORS, Encryption.Encrypt(strvisitor, true, uniqkey));
                string ddatavisitor1 = Encryption.Encrypt(textBox10.Text, true, uniqkey);
                list.Add(ddatavisitor1.Substring(0, 2));

                StringBuilder builder = new StringBuilder();
                foreach (string cat in list)
                {
                    builder.Append(cat).Append("");
                }
                string result = builder.ToString();
                string arg = "License Number=" + result;
                string license = String.Format(arg, true);
                Licensenumber = license;

                string date = dateTimePicker1.Value.ToString("yyyyMMdd");
                string time = String.Format(VALIDTO, Encryption.Encrypt(date.ToString(), true, uniqkey));

                string[] filetxt = new string[] { };
                if (liccountflag > 0)
                {
                    filetxt = FileReader.ReadLines(textBox11.Text);
                }
                try
                {
                    using (StreamWriter stream = new StreamWriter(textBox11.Text, false))
                    {
                        stream.WriteLine("[LicenseCounts]");
                        stream.WriteLine(dataCompUniqnr);
                        stream.WriteLine(dataCompanieName);
                        stream.WriteLine(dataCountUsers);
                        stream.WriteLine(dataCountDoors);
                        stream.WriteLine(dataZones);
                        stream.WriteLine(dataCompanies);
                        stream.WriteLine(dataWorktime);
                        stream.WriteLine(dataportal);
                        stream.WriteLine(datasmart);
                        stream.WriteLine(datavideo);
                        stream.WriteLine(datavisitor);
                        stream.WriteLine(license);
                        stream.WriteLine(time);
                        stream.WriteLine("");
                        int chkflag = 0;
                        for (int i = 0; i < filetxt.Length; i++)
                        {
                            if (filetxt[i].Contains("[CountRequest]"))
                            {
                                chkflag = 1;
                            }
                            if (chkflag == 1)
                            {
                                stream.WriteLine(filetxt[i]);
                            }
                        }
                        stream.Close();
                    }
                    MessageBox.Show("OK!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Invalid password");
            }
        }

        public string identifier(string wmiClass, string wmiProperty)
        //Return a hardware identifier
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                //Only get the first one
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }

        public void GetSerFlashDisk()
        {
            string diskName = string.Empty;
            string testser = string.Empty;
            var numint = string.Empty;
            StringBuilder volumename = new StringBuilder(256);
            path = textBox11.Text;
            deviceIDflash = string.Empty;
            try
            {
                foreach (ManagementObject drive in new ManagementObjectSearcher("select * from Win32_DiskDrive where InterfaceType='USB'").Get())
                {
                    foreach (System.Management.ManagementObject partition in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" + drive["DeviceID"] + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition").Get())
                    {
                        foreach (System.Management.ManagementObject disk in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" + partition["DeviceID"] + "'} WHERE AssocClass = Win32_LogicalDiskToPartition").Get())
                        {
                            diskName = disk["Name"].ToString().Trim();
                            testser = disk["VolumeSerialNumber"].ToString().Trim();
                            numint = Convert.ToInt64(testser, 16).ToString();
                            path = System.IO.Path.Combine(diskName + @"\FoxSecLicense.ini").ToString();

                            if (partition != null)
                            {
                                ManagementObject logical = new ManagementObjectSearcher(String.Format(
                                    "associators of {{Win32_DiskPartition.DeviceID='{0}'}} where AssocClass = Win32_LogicalDiskToPartition",
                                    partition["DeviceID"])).First();

                                if (logical != null)
                                {
                                    List<string> list = new List<string>();

                                    ManagementObject volume = new ManagementObjectSearcher(String.Format(
                                        "select FreeSpace, Size from Win32_LogicalDisk where Name='{0}'",
                                        logical["Name"])).First();

                                    UsbDisk diskn = new UsbDisk(logical["Name"].ToString());

                                    string pnpdeviceid = parseSerialFromDeviceID(drive["PNPDeviceID"].ToString().Trim());
                                    var conpnp = pnpdeviceid.Substring(0, 5);

                                    var conpnpn = converttoascii(conpnp);
                                    var pnpdevidint = Convert.ToUInt64(conpnpn, 16).ToString();

                                    list.Add(pnpdevidint.Substring(0, 4));

                                    diskn.Size = (ulong)volume["Size"];
                                    string size = diskn.Size.ToString();
                                    size = volume["Size"].ToString();
                                    list.Add(size.Substring(0, 4));
                                    list.Add(numint.Substring(0, 7));
                                    string str = "f";
                                    string flashser = Encrypt(str, true);
                                    list.Add(flashser);

                                    StringBuilder builder = new StringBuilder();
                                    foreach (string cat in list)
                                    {
                                        builder.Append(cat).Append("");
                                    }
                                    string result = builder.ToString();
                                    deviceIDflash = result;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                MessageBox.Show(error);
            }
        }

        public void GetSerFlashDisk1()
        {
            string diskName = string.Empty;
            string testser = string.Empty;
            var numint = string.Empty;
            StringBuilder volumename = new StringBuilder(256);
            path = textBox11.Text;
            drivename = (path == "" || path == null) ? "" : path.Substring(0, 2);
            deviceIDflash = string.Empty;
            try
            {
                foreach (ManagementObject drive in new ManagementObjectSearcher("select * from Win32_DiskDrive where InterfaceType='USB'").Get())
                {
                    foreach (System.Management.ManagementObject partition in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" + drive["DeviceID"] + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition").Get())
                    {
                        foreach (System.Management.ManagementObject disk in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" + partition["DeviceID"] + "'} WHERE AssocClass = Win32_LogicalDiskToPartition").Get())
                        {
                            diskName = disk["Name"].ToString().Trim();

                            if (diskName.ToLower() == drivename.ToLower())
                            {
                                testser = disk["VolumeSerialNumber"].ToString().Trim();
                                numint = Convert.ToInt64(testser, 16).ToString();
                                path = System.IO.Path.Combine(diskName + @"\FoxSecLicense.ini").ToString();

                                if (partition != null)
                                {
                                    // associate partitions with logical disks (drive letter volumes)
                                    ManagementObject logical = new ManagementObjectSearcher(String.Format(
                                        "associators of {{Win32_DiskPartition.DeviceID='{0}'}} where AssocClass = Win32_LogicalDiskToPartition",
                                        partition["DeviceID"])).First();

                                    if (logical != null)
                                    {
                                        List<string> list = new List<string>();

                                        ManagementObject volume = new ManagementObjectSearcher(String.Format(
                                            "select FreeSpace, Size from Win32_LogicalDisk where Name='{0}'",
                                            logical["Name"])).First();

                                        UsbDisk diskn = new UsbDisk(logical["Name"].ToString());

                                        string pnpdeviceid = parseSerialFromDeviceID(drive["PNPDeviceID"].ToString().Trim());
                                        var conpnp = pnpdeviceid.Substring(0, 5);

                                        var conpnpn = converttoascii(conpnp);
                                        var pnpdevidint = Convert.ToUInt64(conpnpn, 16).ToString();

                                        list.Add(pnpdevidint.Substring(0, 4));
                                        diskn.Size = (ulong)volume["Size"];
                                        string size = diskn.Size.ToString();
                                        size = volume["Size"].ToString();
                                        list.Add(size.Substring(0, 4));
                                        list.Add(numint.Substring(0, 7));

                                        string str = "f";
                                        string flashser = Encrypt(str, true);
                                        list.Add(flashser);

                                        StringBuilder builder = new StringBuilder();
                                        foreach (string cat in list)
                                        {
                                            builder.Append(cat).Append("");
                                        }
                                        string result = builder.ToString();
                                        deviceIDflash = result;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                MessageBox.Show(error);
            }
        }

        string converttoascii(string text)
        {
            string asciitxt = "";
            for (var i = 0; i < text.Length; i++)
            {
                char current = text[i];
                if (!(Char.IsDigit(current)))
                {
                    asciitxt = asciitxt + Convert.ToString((int)(current));
                }
                else
                {
                    asciitxt = asciitxt + current;
                }
            }
            return asciitxt;
        }

        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            // Get the key from config file
            //   string key = (string)settingsReader.GetValue(ENCRYPTION_KEY, typeof(String));
            //System.Windows.Forms.MessageBox.Show(key);
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(ENCRYPTION_KEY));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(ENCRYPTION_KEY);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public void GetSerHardDisk()
        {
            try
            {
                path = System.AppDomain.CurrentDomain.BaseDirectory + "FoxSecLicense.ini";
                string serial = "";
                List<string> list = new List<string>();
                string model = "";
                ManagementObjectSearcher moSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
                long totalSize = 0;
                deviceID = string.Empty;
                foreach (ManagementObject wmi_HD in moSearcher.Get())
                {
                    if (wmi_HD.Properties["InterfaceType"].Value.ToString() != "USB")
                    {
                        model = wmi_HD["Model"].ToString();  //Model Number
                        try
                        {
                            serial = wmi_HD.GetPropertyValue("SerialNumber").ToString();
                        }
                        catch
                        {
                            serial = identifier("Win32_NetworkAdapterConfiguration", "MacAddress");
                        }
                        totalSize += Convert.ToInt64(wmi_HD.Properties["Size"].Value.ToString());
                    }
                }

                byte[] ba = System.Text.Encoding.ASCII.GetBytes(model);
                string ba0 = ba[0].ToString();
                string ba1 = ba[1].ToString();
                string ba2 = ba[2].ToString();

                long intba0 = Convert.ToInt64(ba0) % 10;
                long intba1 = Convert.ToInt64(ba1) % 10;
                long intba2 = Convert.ToInt64(ba2) % 10;
                string intstrba0 = intba0.ToString();
                string intstrba1 = intba1.ToString();
                string intstrba2 = intba2.ToString();

                list.Add(intstrba0);
                list.Add(intstrba1);
                list.Add(intstrba2);

                string name = identifier("Win32_LogicalDisk", "Name");

                //string Size = identifier("Win32_DiskDrive", "Size");
                string Size = Convert.ToString(totalSize);
                list.Add(Size.Substring(0, 5)); //Jelena Ver67          

                // string serial = identifier("Win32_DiskDrive", "SerialNumber");
                String numint = serial.Substring(0, 6); //Jelena Ver67

                byte[] baser = System.Text.Encoding.ASCII.GetBytes(serial);
                string baser0 = baser[0].ToString();
                string baser1 = baser[1].ToString();
                string baser2 = baser[2].ToString();
                string baser3 = baser[3].ToString();
                string baser4 = baser[4].ToString();
                string baser5 = baser[5].ToString();
                string baser6 = baser[6].ToString();
                // string baser7 = baser[7].ToString();     //Jelena Ver67

                int intbaser0 = Convert.ToInt32(baser0) % 10;
                int intbaser1 = Convert.ToInt32(baser1) % 10;
                int intbaser2 = Convert.ToInt32(baser2) % 10;
                int intbaser3 = Convert.ToInt32(baser3) % 10;
                int intbaser4 = Convert.ToInt32(baser4) % 10;
                int intibaser5 = Convert.ToInt32(baser5) % 10;
                int intbaser6 = Convert.ToInt32(baser6) % 10;
                //int intbaser7 = Convert.ToInt32(baser7) % 10;  //Jelena Ver67

                string intser0 = intbaser0.ToString();
                string intser1 = intbaser1.ToString();
                string intser2 = intbaser2.ToString();
                string intser3 = intbaser3.ToString();
                string intser4 = intbaser4.ToString();
                string intser5 = intibaser5.ToString();
                string intser6 = intbaser6.ToString();
                // string intser7 = intbaser7.ToString();    //Jelena Ver67

                string str = "h";
                string hardser = Encryption.Encrypt(str, true, ENCRYPTION_KEY);

                list.Add(intser0);
                list.Add(intser1);
                list.Add(intser2);
                list.Add(intser3);
                list.Add(intser4);
                list.Add(intser5);
                list.Add(intser6);

                list.Add(hardser);
                // list.Add(intser7);   //Jelena Ver67

                StringBuilder builder = new StringBuilder();
                foreach (string cat in list) // Loop through all strings
                {
                    builder.Append(cat).Append(""); // Append string to StringBuilder
                }
                string result = builder.ToString();

                deviceID = result;
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                MessageBox.Show(error);
            }
        }
        private string parseSerialFromDeviceID(string deviceId)
        {
            string[] splitDeviceId = deviceId.Split('\\');
            string[] serialArray; string serial;
            int arrayLen = splitDeviceId.Length - 1;
            serialArray = splitDeviceId[arrayLen].Split('&');
            serial = serialArray[0]; return serial;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            bool chkpath = checkpath();
            if (chkpath == false)
            {
                MessageBox.Show("Entered license path doesn't exists!!");
                return;
            }
            ReadLicense();
        }
        public void ReadLicense()
        {
            path = textBox11.Text;
            Int32 i = default(Int32);
            Int32 intUniqNrIdx = default(Int32);
            Int32 intEncryptLicNrIdx = default(Int32);
            string strUniqNr = string.Empty;
            string strEncryptLicNr = string.Empty;
            string decryptedLicense = string.Empty;
            string line = string.Empty;
            // string sername;
            string[] lines;
            string newPassword = textBox6.Text; // = "hard12";
            string uniqid = "";
            if (!string.IsNullOrEmpty(confirmPassword) && !string.IsNullOrEmpty(confirmPassword) && newPassword.Equals(confirmPassword))
            {
                try
                {
                    CompUniqNr = TAtest.ReadCompNr(comnr, path);
                    if (CompUniqNr == "")
                    {
                        MessageBox.Show("License does not exists because CompUniqNr not generated for this licence request!!");
                        return;
                    }
                    uniqid = ENCRYPTION_KEY + CompUniqNr;
                    Users = TAtest.Readlicense(user, path, uniqid);
                    Doors = TAtest.Readlicense(door, path, uniqid);
                    Zones = TAtest.Readlicense(zones, path, uniqid);
                    Companies = TAtest.Readlicense(companies, path, uniqid);
                    TimeAndAttendense = TAtest.Readlicense(worktime, path, uniqid);
                    Terminals = TAtest.Readlicense(portal, path, uniqid);
                    Smart = TAtest.Readlicense(smart, path, uniqid);
                    Video = TAtest.Readlicense(video, path, uniqid);
                    Visitors = TAtest.Readlicense(visitor, path, uniqid);
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("Bad Data"))
                    {
                        MessageBox.Show(e.Message + "\r\nor CompUniqNr has been changed.\r\n" + "\r\nor [LicenseCounts] and [CountRequest] both should have same CompUniqNr!!");
                    }
                    else
                    {
                        MessageBox.Show(e.Message + "\r\nor Please check [LicenseCounts] exists in the licence file or not!!");
                    }
                    return;
                }
                getcompname(uniqid);
                if (Users == -1) { textBox1.Text = ""; }
                else { textBox1.Text = Users.ToString(); }

                if (Doors == -1) { textBox2.Text = ""; }
                else { textBox2.Text = Doors.ToString(); }

                if (Zones == -1) { textBox3.Text = ""; }
                else { textBox3.Text = Zones.ToString(); }

                if (Companies == -1) { textBox4.Text = ""; }
                else { textBox4.Text = Companies.ToString(); }

                if (TimeAndAttendense == -1) { textBox5.Text = ""; }
                else { textBox5.Text = TimeAndAttendense.ToString(); }

                if (Video == -1) { textBox9.Text = ""; }
                else { textBox9.Text = Video.ToString(); }

                if (Terminals == -1) { textBox8.Text = ""; }
                else { textBox8.Text = Terminals.ToString(); }

                if (Smart == -1) { textBox12.Text = ""; }
                else { textBox12.Text = Smart.ToString(); }

                if (Visitors == -1) { textBox10.Text = ""; }
                else { textBox10.Text = Visitors.ToString(); }

                textBox11.Text = path;

                lines = FileReader.ReadLines(path);
                for (i = 0; i < lines.Length; i++)
                {
                    line = lines[i];
                    if (line.Contains("[CountRequest]"))
                    {
                        break;
                    }
                    else
                    {
                        if (line.Length > 15)
                        {
                            if (string.Compare(line.Substring(0, 16), "EncryptLicenceNr", true) == 0)
                            {
                                intEncryptLicNrIdx = i;
                                strEncryptLicNr = line.Substring(17, line.Length - 17);
                                Encryption.Decrypt(line, true, uniqid);
                            }
                            if (line.Contains("CompUniqNr"))
                            {
                                string datacomnr = Searcher.TrimEnd(line, "CompUniqNr=");
                                if (line.Contains("CompanieName="))
                                {
                                    string data = Searcher.TrimEnd(line, "CompanieName=");
                                    textBox7.Text = Encryption.Decrypt(data, true, uniqid);
                                }
                                if (deviceIDflash == string.Empty)
                                {
                                    datacomnr = deviceID;
                                    string str = "CompUniqNr=" + datacomnr;
                                    string text = File.ReadAllText(path);
                                    text = text.Replace(line, str);
                                }
                                else
                                {
                                }
                            }
                            if (line.Contains("ValidTo"))
                            {
                                string data = Searcher.TrimEnd(line, "ValidTo=");
                                string dateNow = Encryption.Decrypt(data, true, uniqid);
                                DateTime result;
                                if (DateTime.TryParseExact(dateNow, "yyyyMMdd", CultureInfo.CurrentCulture, DateTimeStyles.None, out result))
                                {
                                    dateTimePicker1.Value = result;
                                }
                            }
                        }
                    }
                    if (intEncryptLicNrIdx > 0 & intUniqNrIdx > 0)
                        break;
                }

            }
            else
            {
                MessageBox.Show("Invalid password");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        protected static void RecognizeAppState()
        {
            try
            {
                string rTime = "";
                if (Registry.GetValue("HKEY_LOCAL_MACHINE\\System\\Inith", "1", null) == null)
                {
                    rTime = null;
                }
                else
                {
                    rTime = Registry.GetValue("HKEY_LOCAL_MACHINE\\System\\Inith", "1", null).ToString();
                }

                if (!String.IsNullOrEmpty(rTime))
                {
                    if (rTime.Length > 9)
                    {
                        if (Digits(rTime))
                        {
                            if (ValidateDate(String.Concat(
                                "20" + rTime.Substring(4, 2), "-",
                                rTime.Substring(8, 2), "-",
                                rTime.Substring(0, 2))))
                            {
                                rTime =
                                   "20" + rTime.Substring(4, 2) +
                                   rTime.Substring(8, 2) +
                                   rTime.Substring(0, 2) +
                                   rTime.Substring(2, 2) +
                                   rTime.Substring(6, 2);
                            }
                            else
                                return;
                        }
                    }
                    else
                    {
                        return;
                    }

                    DateTime currentTime = DateTime.Now;
                    DateTime rTimeDate = DateTime.ParseExact(rTime, "yyyyMMddHHmm", null);
                    TimeSpan span = currentTime.Subtract(rTimeDate);

                    if (span.TotalHours > 2)
                        appIsLocked = false;
                    else
                        appIsLocked = true;
                }
                else
                {
                    //siia tuleb kontroll , kas registris on kirje et oli lukustatud.
                    //kui selline kirje on ja rTime=Nothing siis on võimalik et registrist kustutati rTime
                    appIsLocked = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        internal static bool ValidateDate(string yyyyMMdd)
        {
            return dateRegex.IsMatch(yyyyMMdd);
        }
        internal static bool Digits(params string[] values)
        {
            bool valid = ValidateString(digitsRegex, values);
            return valid;
        }
        internal static bool ValidateString(Regex r, params string[] values)
        {
            if (r == null)
                return false;

            bool valid = true;
            foreach (string s in values)
            {
                valid &= r.IsMatch(s);
                if (!valid)
                    return false;
            }
            return valid;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text = "100";
            textBox2.Text = "10";
            textBox3.Text = "50";
            textBox4.Text = "1";
            textBox5.Text = "0";
            textBox10.Text = "0";
            textBox9.Text = "0";
            textBox8.Text = "0";
            textBox12.Text = "0";
            dateTimePicker1.Value = DateTime.Today.AddYears(1000);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog
            {
                CheckFileExists = false,
                CheckPathExists = false,
                DefaultExt = "ini",
                Filter = "ini files (*.ini)|*.ini",
                FilterIndex = 2,
                RestoreDirectory = false,
                ReadOnlyChecked = true,
                ShowReadOnly = true
            };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox11.Text = openFileDialog1.FileName;
            }
        }

        public void getcompname(string uniqid)
        {
            //  var currentpath = System.IO.Path.Combine(Install.projectsPath + @"\FoxSecConf\SecConf.ini");
            if (File.Exists(path))
            {
                string[] lines = FileReader.ReadLines(path);
                for (int i = 0; i < lines.Length; i++)
                {
                    string line = lines[i];
                    if (line.Contains("[CountRequest]"))
                    {
                        break;
                    }
                    else
                    {
                        if (line.Length > 10)
                        {
                            if (line.Contains("CompanieName="))
                            {
                                string datacomnr = Searcher.TrimEnd(line, "CompanieName=");
                                string[] arr = datacomnr.Split('\\');
                                string compnname = arr[arr.Length - 1];
                                compnname = compnname.Split('.')[0];
                                compnname = Encryption.Decrypt(compnname, true, uniqid);
                                textBox7.Text = compnname;
                            }
                        }
                    }
                }
            }
            else
            {
                textBox7.Text = "";
            }
        }

        public void getcompnameRequest()
        {
            //  var currentpath = System.IO.Path.Combine(Install.projectsPath + @"\FoxSecConf\SecConf.ini");
            if (File.Exists(path))
            {
                string[] lines = FileReader.ReadLines(path);
                for (int i = 0; i < lines.Length; i++)
                {
                    string line = lines[i];
                    if (line.Length > 10)
                    {
                        if (line.Contains("CompanieName="))
                        {
                            string datacomnr = Searcher.TrimEnd(line, "CompanieName=");
                            string[] arr = datacomnr.Split('\\');
                            string compnname = arr[arr.Length - 1];
                            compnname = compnname.Split('.')[0];
                            textBox7.Text = compnname;
                        }
                    }
                }
            }
            else
            {
                textBox7.Text = "";
            }
        }
        private void button6_Click(object sender, EventArgs e)
        {
            ReadRequest();
        }
        private void ReadRequest()
        {
            bool chkpath = checkpath();
            if (chkpath == false)
            {
                MessageBox.Show("Entered license path doesn't exists!!");
                return;
            }
            path = textBox11.Text;
            string strUniqNr = string.Empty;
            string strEncryptLicNr = string.Empty;
            string decryptedLicense = string.Empty;
            string line = string.Empty;
            // string sername;
            string newPassword = textBox6.Text;//"hard12"            

            if (!string.IsNullOrEmpty(confirmPassword) && !string.IsNullOrEmpty(confirmPassword) && newPassword.Equals(confirmPassword))
            {
                int tc = TAtest.checkrequestexist(path, "[CountRequest]");
                if (tc == 0)
                {
                    textBox6.Text = "";
                    MessageBox.Show("No licence request found in FoxSecLicense.ini file!!");
                    return;
                }
                else
                {
                    try
                    {
                        CompUniqNr = TAtest.ReadValidTo(comnr, path);
                        if (CompUniqNr == "")
                        {
                            //MessageBox.Show("License does not exists because CompUniqNr not generated for this licence request!!");
                            DialogResult result = MessageBox.Show("Do you want to generate Unique Id from drive?", "Generate", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                            if (result.Equals(DialogResult.OK))
                            {
                                string devid = "";
                                GetSerFlashDisk();
                                if (deviceIDflash == string.Empty)
                                { GetSerHardDisk(); }
                                if (deviceID == String.Empty)
                                {
                                    devid = deviceIDflash;
                                }
                                else
                                {
                                    devid = deviceID;
                                }
                                label16.Text = devid;
                                string[] lines = FileReader.ReadLines(path);
                                try
                                {
                                    using (StreamWriter stream = new StreamWriter(textBox11.Text, false))
                                    {
                                        for (int i = 0; i < lines.Length; i++)
                                        {
                                            line = lines[i];
                                            if (line.Contains("CompUniqNr"))
                                            {
                                                line = "CompUniqNr=" + devid;
                                                stream.WriteLine(line);
                                            }
                                            else
                                            {
                                                stream.WriteLine(line);
                                            }
                                        }
                                        stream.Close();

                                    }
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                    return;
                                }
                            }
                            else
                            {
                                return;
                            }
                        }
                        Users = TAtest.ReadRequest(user, path);
                        Doors = TAtest.ReadRequest(door, path);
                        Zones = TAtest.ReadRequest(zones, path);
                        Companies = TAtest.ReadRequest(companies, path);
                        TimeAndAttendense = TAtest.ReadRequest(worktime, path);
                        Terminals = TAtest.ReadRequest(portal, path);
                        Smart = TAtest.ReadRequest(smart, path);
                        Video = TAtest.ReadRequest(video, path);
                        Visitors = TAtest.ReadRequest(visitor, path);
                        ValidTo = TAtest.ReadValidTo(validto, path);
                        CompUniqNr = TAtest.ReadValidTo(comnr, path);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        return;
                    }
                    getcompnameRequest();
                    if (Users == -1) { textBox1.Text = ""; }
                    else { textBox1.Text = Users.ToString(); }

                    if (Doors == -1) { textBox2.Text = ""; }
                    else { textBox2.Text = Doors.ToString(); }

                    if (Zones == -1) { textBox3.Text = ""; }
                    else { textBox3.Text = Zones.ToString(); }

                    if (Companies == -1) { textBox4.Text = ""; }
                    else { textBox4.Text = Companies.ToString(); }

                    if (TimeAndAttendense == -1) { textBox5.Text = ""; }
                    else { textBox5.Text = TimeAndAttendense.ToString(); }

                    if (Terminals == -1) { textBox8.Text = ""; }
                    else { textBox8.Text = Terminals.ToString(); }

                    if (Smart == -1) { textBox12.Text = ""; }
                    else { textBox12.Text = Smart.ToString(); }

                    if (Video == -1) { textBox9.Text = ""; }
                    else { textBox9.Text = Video.ToString(); }

                    if (Visitors == -1) { textBox10.Text = ""; }
                    else { textBox10.Text = Visitors.ToString(); }

                    if (ValidTo == "-1") { dateTimePicker1.Text = ""; }
                    else { dateTimePicker1.Text = ValidTo.ToString(); }

                    if (CompUniqNr == "-1") { label16.Text = ""; }
                    else { label16.Text = CompUniqNr.ToString(); }

                    linreqflag = 1;
                    validateread = 1;
                    //textBox6.Text = "";
                }
            }

            else
            {
                MessageBox.Show("Invalid password");
            }
        }
        private void button7_Click(object sender, EventArgs e)
        {
            path = textBox11.Text;
            string newPassword = textBox6.Text;//"hard12"
            string confirmPassword = "hard12";

            List<string> list = new List<string>();

            if (!string.IsNullOrEmpty(confirmPassword) && !string.IsNullOrEmpty(confirmPassword) && newPassword.Equals(confirmPassword))
            {
                liccountflag = TAtest.checkrequestexist(path, "[LicenseCounts]");
                string dataCompUniqnr = string.Empty;
                string dataCompUniqnr1 = string.Empty;
                string uniqkey = "";

                GetSerFlashDisk1();
                if (deviceIDflash == string.Empty)
                { GetSerHardDisk(); }
                if (deviceID == String.Empty)
                {
                    dataCompUniqnr = String.Format("CompUniqNr=" + deviceIDflash, true);
                    uniqkey = ENCRYPTION_KEY + dataCompUniqnr.Replace("CompUniqNr=", "");
                    dataCompUniqnr1 = Encryption.Encrypt(deviceIDflash, true, uniqkey);
                    list.Add(dataCompUniqnr1.Substring(0, 2));
                }
                else
                {
                    dataCompUniqnr = String.Format("CompUniqNr=" + deviceID, true);
                    uniqkey = ENCRYPTION_KEY + dataCompUniqnr.Replace("CompUniqNr=", "");
                    dataCompUniqnr1 = Encryption.Encrypt(deviceID, true, uniqkey);
                    list.Add(dataCompUniqnr1.Substring(0, 2));
                }

                string strcompanename = textBox7.Text;
                string dataCompanieName = String.Format(COMPANIE_NAME, Encryption.Encrypt(strcompanename, true, uniqkey));
                string dataCompanieName1 = Encryption.Encrypt(textBox7.Text, true, uniqkey);
                list.Add(dataCompanieName1.Substring(0, 2));

                string struser = "users" + textBox1.Text;
                string dataCountUsers = String.Format(COUNT_CountUsers, Encryption.Encrypt(struser, true, uniqkey));
                string dataCountUsers1 = Encryption.Encrypt(textBox1.Text, true, uniqkey);
                list.Add(dataCountUsers1.Substring(0, 2));

                string strdoor = "doors" + textBox2.Text;
                string dataCountDoors = String.Format(COUNT_CountDoors, Encryption.Encrypt(strdoor, true, uniqkey));
                string dataCountDoors1 = Encryption.Encrypt(textBox2.Text, true, uniqkey);
                list.Add(dataCountDoors1.Substring(1, 3));

                string strzones = "zones" + textBox3.Text;
                string dataZones = String.Format(ZONES, Encryption.Encrypt(strzones, true, uniqkey));
                string dataZones1 = Encryption.Encrypt(textBox3.Text, true, uniqkey);
                list.Add(dataZones1.Substring(2, 4));

                string dataCompanies = String.Format(COMPANIES, Encryption.Encrypt(textBox4.Text, true, uniqkey));
                string dataCompanies1 = Encryption.Encrypt(textBox4.Text, true, uniqkey);
                list.Add(dataCompanies1.Substring(1, 3));

                string strWorktime = "timeandattendense" + textBox5.Text;
                string dataWorktime = String.Format(WORKTIME, Encryption.Encrypt(strWorktime, true, uniqkey));
                string ddataWorktime1 = Encryption.Encrypt(textBox5.Text, true, uniqkey);
                list.Add(ddataWorktime1.Substring(0, 2));

                string strportal = "terminals" + textBox8.Text;
                string dataportal = String.Format(PORTAL, Encryption.Encrypt(strportal, true, uniqkey));
                string ddataportal1 = Encryption.Encrypt(textBox8.Text, true, uniqkey);
                list.Add(ddataportal1.Substring(0, 2));

                string strvideo = "video" + textBox9.Text;
                string datavideo = String.Format(VIDEO, Encryption.Encrypt(strvideo, true, uniqkey));
                string ddatavideo1 = Encryption.Encrypt(textBox9.Text, true, uniqkey);
                list.Add(ddatavideo1.Substring(0, 2));

                string strsmart = "smart" + textBox12.Text;
                string datasmart = String.Format(SMART, Encryption.Encrypt(strsmart, true, uniqkey));
                string ddatasmart1 = Encryption.Encrypt(textBox12.Text, true, uniqkey);
                list.Add(ddatasmart1.Substring(0, 2));

                string strvisitor = "visitors" + textBox10.Text;
                string datavisitor = String.Format(VISITORS, Encryption.Encrypt(strvisitor, true, uniqkey));
                string ddatavisitor1 = Encryption.Encrypt(textBox10.Text, true, uniqkey);
                list.Add(ddatavisitor1.Substring(0, 2));

                StringBuilder builder = new StringBuilder();
                foreach (string cat in list)
                {
                    builder.Append(cat).Append("");
                }
                string result = builder.ToString();
                string arg = "License Number=" + result;
                string license = String.Format(arg, true);
                Licensenumber = license;

                string date = dateTimePicker1.Value.ToString("yyyyMMdd");
                string time = String.Format(VALIDTO, Encryption.Encrypt(date.ToString(), true, uniqkey));
                try
                {
                    using (StreamWriter stream = new StreamWriter(textBox11.Text, false))
                    {
                        stream.WriteLine("[LicenseCounts]");
                        stream.WriteLine(dataCompUniqnr);
                        stream.WriteLine(dataCompanieName);
                        stream.WriteLine(dataCountUsers);
                        stream.WriteLine(dataCountDoors);
                        stream.WriteLine(dataZones);
                        stream.WriteLine(dataCompanies);
                        stream.WriteLine(dataWorktime);
                        stream.WriteLine(dataportal);
                        stream.WriteLine(datasmart);
                        stream.WriteLine(datavideo);
                        stream.WriteLine(datavisitor);
                        stream.WriteLine(license);
                        stream.WriteLine(time);
                        stream.WriteLine("");
                        stream.Close();
                    }
                    MessageBox.Show("OK!");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Invalid password");
            }
        }

        private bool checkpath()
        {
            if (File.Exists(textBox11.Text))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool checkwritefilepath()
        {
            string licpath = textBox11.Text;
            licpath = licpath.Replace("FoxSecLicense.ini", "");
            if (Directory.Exists(licpath))
            {
                if (!File.Exists(textBox11.Text))
                {
                    File.Create(textBox11.Text).Close();
                }

                return true;
            }
            else
            {
                return false;
            }
        }
        private void button8_Click(object sender, EventArgs e)
        {
            string newPassword = textBox6.Text;//"hard12"
            string confirmPassword = "hard12";
            if (!string.IsNullOrEmpty(confirmPassword) && !string.IsNullOrEmpty(confirmPassword) && newPassword.Equals(confirmPassword))
            {
                path = textBox11.Text;
                string CompUniqnr = string.Empty;
                GetSerFlashDisk1();
                if (deviceIDflash == string.Empty)
                { GetSerHardDisk(); }
                if (deviceID == String.Empty)
                {
                    CompUniqnr = deviceIDflash;
                }
                else
                {
                    CompUniqnr = deviceID;
                }
                label16.Text = CompUniqnr;
                string line = string.Empty;
                string[] lines = FileReader.ReadLines(path);
                try
                {
                    using (StreamWriter stream = new StreamWriter(textBox11.Text, false))
                    {
                        for (int i = 0; i < lines.Length; i++)
                        {
                            line = lines[i];
                            if (line.Contains("CompUniqNr"))
                            {
                                line = "CompUniqNr=" + CompUniqnr;
                                stream.WriteLine(line);
                            }
                            else
                            {
                                stream.WriteLine(line);
                            }
                        }
                        stream.Close();
                    }
                    MessageBox.Show("New Unique Nr generated successfully..");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return;
                }
            }
            else
            {
                MessageBox.Show("Invalid password");
            }
        }
    }
}
